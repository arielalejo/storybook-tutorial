## For running snapshot tests

1. first install addons:
`npm i --save-dev @storybook/addon-storyshots @storybook/addon-storyshots-puppeteer`

2.  configure a file `storyshots.test.js`

3.  run initial storyshots
`npm run test`

4. Change some arguments in `Button.stories.js `
and run the tests, you will see the test fails.

5. snaptshots will be stored in `tests/__image_snapshots__` folder