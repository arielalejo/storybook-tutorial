const {checkA11y, injectAxe} = require('axe-playwright');

module.exports = {
    async preRender(page, context){
        await injectAxe(page)
    },
    async postRender(page, context){
        await checkA11y(page, '#root', {
            detailedReport: true,
            detailedReportOptions: {
                html: true
            }
        });

        const accessibilityTree = await page.accessibility.snapshot()
        // representation of how the page is interpretated by assitive technologies
        expect(accessibilityTree).toMatchSnapshot()
    }
}