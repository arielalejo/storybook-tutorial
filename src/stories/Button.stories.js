import Button from "../components/Button";

export default { // the storybook
    title: "Button",
    component: Button,
    argTypes: {handleClick: {
        action: "handleClickAction"
    }}
}

// add controls (intearctivity in the browser)
const Template = args => <Button {...args}/>

// export specific storybook
export const Red = Template.bind({})
Red.args = {
    backgroundColor: "red",
    label: "Press me!",
    size: "md"
}

// export specific storybook
export const Green = Template.bind({})
Green.args = {
    backgroundColor: "magenta",
    label: "Press me!",
    size: "md"
}

// export specific storybook
export const Small = Template.bind({})
Small.args = {
    backgroundColor: "yellow",
    label: "Press me!",
    size: "sm"
}