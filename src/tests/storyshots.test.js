import initStoryShots from '@storybook/addon-storyshots'
import { imageSnapshot} from "@storybook/addon-storyshots-puppeteer";

initStoryShots({
    suite: 'storyshots',
    test: imageSnapshot({
        storybookUrl: 'http://localhost:6070'
    })
})